var through = require("through2");
var path = require("path");
var ejs = require("ejs");
var fs = require("fs");
var grayMatter = require("gray-matter");
var slug = require("slug");
var Vinyl = require("vinyl");

module.exports = function(params)
{
    // Parse and figure out all the parameters.
    if (!params) params = {};
    params.relativePath = params.relativePath || "categories/";
    params.property = params.property || "categories";

    // We need to compile the template.
    var templateFilename = params.ejsTemplate;
    var template = fs.readFileSync(templateFilename).toString();
    var render = ejs.compile(template);

    // Set up the scanner that looks for existing categories but also figures
    // out which ones we'll need. In the finalize function, we'll create the
    // missing entries.
    var slugsSeen = {};
    var needed = {};

    var pipe = through.obj(
        function(file, encoding, callback)
        {
            // Go through and gather up all the tags needed.
            if (file.data.page[params.property])
            {
                for (var item of file.data.page[params.property])
                {
                    needed[item] = true;
                }
            }

            // Keep track of the slug so we don't create a file that we've
            // already seen. We only do this for files that start with the
            // directory (and then we remove it).
            var path = file.data.relativePath;

            if (path.startsWith(params.relativePath) && path !== params.relativePath)
            {
                path = path.substring(params.relativePath.length);
                slugsSeen[path] = true;
            }

            // We are finish scanning the file.
            return callback(null, file);
        },
        function(callback)
        {
            // Loop through all the needed categories to figure out which ones
            // have to be generated.
            for (var need in needed)
            {
                // If we've already seen it, then skip it.
                var needSlug = slug(need, {lower: true}) + "/";

                if (slugsSeen.hasOwnProperty(needSlug))
                {
                    continue;
                }

                // Figure out the filename we'll be writing.
                var filename = path.join(
                    params.directory,
                    needSlug,
                    "index.html");

                // At this point, we need to create the file. We do this by
                // creating the file using the EJS template with the only
                // parameter (name) with the name.
                var results = render({ name: need });

                // Pull out the YAML header so we can reconstruct what other
                // files have at this point.
                var matter = grayMatter(results);

                // Now that we have a file, we have to inject it
                // into the pipeline.
                var file = new Vinyl({
                    cwd: process.cwd(),
                    base: process.cwd(),
                    path: filename,
                    contents: new Buffer(matter.content)
                });

                matter.data.relativePath = path.join(params.relativePath, needSlug);
                file.page = matter.data;
                file.data = {
                    page: matter.data,
                    site: params.site,
                    relativePath: path.join(params.relativePath, needSlug),
                    contents: matter.content
                };

                this.push(file);
            }

            // Finish processing the finalize.
            callback();
        });

    return pipe;
}
